# -*- coding: utf-8 -*
import os
import time

flag_debug = True
wait_scale = 1 # please add more if latency is big

class APPContent():
	def __init__(self, case_name='no-name', retry=1):
		self.case_name = case_name
		self.retry = retry

	def load(self):
		global flag_debug
		os_command('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
		assert os.path.isfile('window_dump.xml')

		with open('window_dump.xml', 'r', encoding="utf-8") as f:
			self.xmlString = f.read()

		if flag_debug:
			move_file('window_dump.xml', '{}.xml'.format(self.case_name))
		else:
			os.remove('window_dump.xml')

	def has_list_text(self, list):
		retry_count = 0
		while retry_count <= self.retry:
			self.load()
			missing = False
			for text in list:
				if not self.has_text(text):
					missing = True

			if not missing:
				return True

			retry_count += 1

		return False

	def has_text(self, text):
		return self.xmlString.find(text) != -1

	def screenshot(self):
		os_command('adb shell screencap -p /sdcard/screen.png')
		os_command('adb pull /sdcard/screen.png')

		target_file = '{}.png'.format(self.case_name)
		move_file('./screen.png', target_file)

def os_command(cmd):
	assert(os.system(cmd) == 0)

def move_file(src, des):
	if os.path.isfile(des):
		os.remove(des)

	os.rename(src, des)

def tap(x, y, delay=0.0):
	global wait_scale

	os_command('adb shell input tap {} {}'.format(x, y))
	time.sleep(int(delay * wait_scale))

def tap_back(delay=0.0):
	global wait_scale

	tap(229, 1848, int(delay * wait_scale))

def drag(x1, y1, x2, y2, duration=1.0):
	global wait_scale

	command = 'adb shell input swipe {} {} {} {} {}'.format(
		x1,
		y1,
		x2,
		y2,
		int(duration * 1000 * wait_scale)
	)
	os_command(command)

def key_input(msg, delay=0.0):
	global wait_scale

	os_command('adb shell input text "{}"'.format(msg))
	os_command('adb shell input keyevent "KEYCODE_ENTER"')
	time.sleep(int(delay * wait_scale))

def disconnect():
	os_command('adb shell svc data disable')
	os_command('adb shell svc wifi disable')

def connect():
	os_command('adb shell svc data enable')
	os_command('adb shell svc wifi enable')

def wait(s):
	global wait_scale
	time.sleep(s * wait_scale)

# [Content] XML Footer Text
def test_has_homepage_node():
	target_text_list = [
		'首頁'
	]
	contex = APPContent('case0-has-homepage')
	assert contex.has_list_text(target_text_list)

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	tap(100, 100, 1)

# 1. [Content] Side Bar Text
def test_has_sidebar_text():
	target_text_list = [
		'查看商品分類',
		'查訂單/退訂退款',
		'追蹤/買過/看過清單',
		'智慧標籤',
		'PChome 旅遊',
		'線上手機回收',
		'給24h購物APP評分'
	]
	appContent = APPContent('case1-has-sidebar')
	assert appContent.has_list_text(target_text_list)

# 2. [Screenshot] Side Bar Text
def test_screenshot_sidebar_text():
	appContent = APPContent('case2-side-bar')
	appContent.screenshot()

# 3. [Context] Categories
def test_categories_text():
	# close sidebar
	tap(980, 121, 1.5)

	# open categories
	drag(678, 1569, 678, 361, 2)
	drag(678, 1569, 678, 361, 2)
	tap(1011, 277, 1.5)

	target_text_list = [
		'精選 ',
		'3C ',
		'周邊 ',
		'NB ',
		'通訊 ',
		'數位 ',
		'家電',
		'日用',
		'食品',
		'生活',
		'運動戶外',
		'美妝',
		'衣鞋包錶'
	]
	appContent = APPContent('case3-categories-text')
	appContent.has_list_text(target_text_list)

# 4. [Screenshot] Categories
def test_screenshot_categories():
	appContent = APPContent('case4-categories')
	appContent.screenshot()

# 5. [Context] Categories page
def test_categories_page_text():
	# close categories
	tap(822, 1264, 1)

	# open sidebar
	tap(100, 100, 1)

	# open categories page
	tap(505, 561, 2)

	target_text_list = [
		'3C',
		'周邊',
		'NB',
		'通訊',
		'數位',
		'家電',
		'日用',
		'食品',
		'生活',
		'運動戶外',
		'美妝',
		'衣鞋包錶'
	]
	appContent = APPContent('case5-categories-page-text', 2)
	assert appContent.has_list_text(target_text_list)

# 6. [Screenshot] Categories page
def test_screenshot_categories_page():
	appContent = APPContent('case6-categories-page')
	appContent.screenshot()

# 7. [Behavior] Search item “switch”
def test_search_item_switch():
	# enter switch into search bar
	tap(600, 135, 2.0)
	key_input('switch', 6.0)

	content = APPContent('case7-behavior-search-switch')
	content.screenshot()
	assert content.has_list_text(['遊戲'])

# 8. [Behavior] Follow an item and it should be add to the list
def test_follow_an_item():
	# select an item
	tap(600, 600, 3)

	# tap follow button
	tap(97, 1701, 3)

	# back to front page
	tap_back(2)
	tap_back(2)
	tap_back(2)

	# to side bar
	tap(100, 100, 5)

	# to follow list
	tap(420, 883, 5)

	content = APPContent('case8-behavior-follow-a-item')
	content.screenshot()
	assert content.has_list_text(['Switch']) or content.has_list_text(['switch'])

# 9. [Behavior] Navigate to the detail of item
def test_navigate_detail_of_item():
	# tap item
	tap(273, 834, 6)

	# to detail
	drag(500, 1460, 500, 340, 1)
	drag(500, 1460, 500, 340, 1)
	tap(537, 132, 6)

	content = APPContent('case9-navigate-detail-of-item')
	content.screenshot()
	assert content.has_list_text(['商品特色'])

	# cancel following
	tap(97, 1701, 5)

# 10. [Screenshot] Disconnetion Screen
def test_screenshot_disconnection_screen():
	disconnect()
	wait(0.5)
	content = APPContent('case10-disconnection-screen')
	content.screenshot()
	connect()